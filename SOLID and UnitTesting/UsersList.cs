﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace SOLID_and_UnitTesting
{
    public class UsersList
    {

        List<User> users = new List<User> { 
        new GuestUser(),
        new AdminUser("admin", "admin")
        };
        public void Add(string name, string pass)
        {
            bool k = true;
            foreach (var item in users)
            {
                if (item.Name == name)
                {
                    k = false;
                    break;
                }
            }
            if (k)
                users.Add(new RegisteredUser(name, pass));
        }
        public void Add(User user)
        {
            bool k = true;
            foreach (var item in users)
            {
                if (item.Name == user.Name)
                {
                    k = false;
                    break;
                }
            }
            if (k)
                users.Add(user);
                users.Add(new RegisteredUser(user.Name, user.Pass));
        }
        public User Search(User user)
        {
            return users.Where(x => x.Name == user.Name).Where(x => x.Pass == user.Pass).FirstOrDefault(); 
        }
        public int Count()
        {
            return users.Count;
        }
        public User SearchByName(string name)
        {
            return users.Where(x => x.Name == name).FirstOrDefault();
        }
        public User SearchByPass(string pass)
        {
            return users.Where(x => x.Pass == pass).FirstOrDefault();
        }
        public void PrintUsersList()
        {
            for (int i = 0; i < users.Count; i++)
            {
                Console.WriteLine();
                Console.WriteLine(i+ ". ");
                users[i].Print();
            }
        }
        public User this[int i]
        {
            get
            {
                return users[i];
            }
        }
    }
}
