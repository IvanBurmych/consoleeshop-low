﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace SOLID_and_UnitTesting
{
    public class User
    {

        public string Name { get; set; }
        public string Pass { get; set; }

        public User(string name, string pass)
        {
            if (name == null || pass == null)
                throw new NullReferenceException();
            if (name.Length == 0 || pass.Length == 0)
                throw new ArgumentException("length ex");
            Name = name;
            Pass = pass;
        }
        public User()
        {

        }
        public virtual (User, string) PrintActions(User user, List<Product> products)
        {
            return (user, "");
        }
        public void ViewProducts(List<Product> products)
        {
            for (int i = 0; i < products.Count; i++)
            {
                if (products[i] == null)
                    throw new ArgumentNullException();
                Console.WriteLine($"{i}.");
                products[i].Print();
            }
        }
        public Product Search(List<Product> products, string str)
        {
            return products.Where(x => x.Name.Contains(str)).FirstOrDefault();
        }
        public void Print()
        {
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Pass: {Pass}");
        }
    }
}
