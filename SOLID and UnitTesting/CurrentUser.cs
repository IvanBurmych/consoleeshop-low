﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID_and_UnitTesting
{
    public class CurrentUser
    {

        public User User { get; set; }
        public List<Product> ProductsList { get; set; }
        public CurrentUser(User user, List<Product> productsList)
        {
            User = user;
            ProductsList = productsList;
        }
    }
}
