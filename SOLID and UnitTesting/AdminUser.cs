﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID_and_UnitTesting
{
    public class AdminUser : User
    {

        public List<Order> Orders { get; set; } = new List<Order>();
        public AdminUser(string name, string pass) : base(name, pass) { }
        public void ViewOrders()
        {
            for (int i = 0; i < Orders.Count; i++)
            {
                if (Orders[i] == null)
                    throw new ArgumentNullException();
                Console.WriteLine($"{i}.");
                Orders[i].Print();
            }
            Console.WriteLine();
        }
        public (User, string) ConfirmOrder()
        {
            Console.WriteLine("To confirm receipt, enter the item number ");
            string str = Console.ReadLine();
            int number;
            bool verif = int.TryParse(str, out number);
            if (number < 0 || number >= Orders.Count)
                return (this, "");
            if (verif)
            {
                Orders[number].Status = "Received";
                return (this, "Received");
            }
            return (this, "");
        }
        public (User, string) CancelOrder(List<Product> products)
        {
            Console.WriteLine("To cancel order, enter the item number ");
            string str = Console.ReadLine();
            int number;
            bool verif = int.TryParse(str, out number);
            if(number < 0 || number >= Orders.Count)
                return (this, "");
            if (verif)
            {
                products.Add(new Product(Orders[number].Name, Orders[number].Category, Orders[number].Description, Orders[number].Cost));
                Orders[number].Status = "Canceled by User";
                Console.WriteLine("Complete");
                return (this, "Canceled by User");
            }
            return (this, "");
        }
        public void AddNewProduct(List<Product> products)
        {
            Console.WriteLine("Enter product name ");
            string name = Console.ReadLine();
            Console.WriteLine("Enter product Category ");
            string category = Console.ReadLine();
            Console.WriteLine("Enter product Description ");
            string description = Console.ReadLine();
            Console.WriteLine("Enter product Cost ");
            string cost = Console.ReadLine();
            decimal costInt;
            bool verif = decimal.TryParse(cost, out costInt);
            if (verif)
            {
                products.Add(new Product(name, category, description, costInt));
            }
        }
        public void СhangeProductValues(List<Product> products)
        {
            Console.WriteLine("To change product values, enter the item number ");
            string str = Console.ReadLine();
            int number;
            bool verif = int.TryParse(str, out number);
            if (verif && number >= 0 && number < Orders.Count)
            {
                Console.WriteLine("To change product name enter '1'");
                Console.WriteLine("To change product Category enter '2'");
                Console.WriteLine("To change product Description enter '3'");
                Console.WriteLine("To change product Cost enter '4'");
                string str1 = Console.ReadLine();
                int number1;
                bool verif1 = int.TryParse(str1, out number1);
                Console.WriteLine("Enter new value");
                if (verif1)
                    switch (number1)
                    {
                        case 1:
                            products[number].Name = Console.ReadLine();
                            break;
                        case 2:
                            products[number].Category = Console.ReadLine();
                            break;
                        case 3:
                            products[number].Description = Console.ReadLine();
                            break;
                        case 4:
                            string costStr = Console.ReadLine();
                            decimal cost;
                            bool costVerif = decimal.TryParse(costStr, out cost);
                            if (costVerif)
                                products[number].Cost = cost;
                            else
                                Console.WriteLine("Wrong cost");

                            break;
                        default:
                            break;
                    }
                else Console.WriteLine("Wrong number");
            }
            else Console.WriteLine("Wrong number");

        }
        public override (User, string) PrintActions(User user, List<Product> products)
        {
            Console.WriteLine("Actions:");
            Console.WriteLine("1. Displaying a list of products");
            Console.WriteLine("2. Product search");
            Console.WriteLine("3. Orders list");
            Console.WriteLine("4. Сonfirm receipt of the item");
            Console.WriteLine("5. Show information about users");
            Console.WriteLine("6. Add new product ");
            Console.WriteLine("7. Сhange product values ");
            Console.WriteLine("8. Сhange the user's order status ");
            Console.WriteLine("9. LogOut");
            Console.WriteLine("Enter the number");
            Console.WriteLine();
            int num;
            string read = Console.ReadLine();
            bool res = int.TryParse(read, out num);
            if (res)
                switch (num)
                {
                    case 1:
                        ViewProducts(products);
                        Console.WriteLine("To place an order, enter the item number ");
                        Console.WriteLine("Enter any word to return ");
                        string str = Console.ReadLine();
                        int number;
                        bool verif = int.TryParse(str, out number);
                        if (number <= 0 || number > products.Count)
                        {
                            break;
                        }
                        else if (verif)
                        {
                            Orders.Add(new Order(products[number]));
                            products.RemoveAt(number);
                            return (user, "NewOrder");
                        }
                        break;

                    case 2:
                        Console.WriteLine("Enter characters");
                        string str1 = Console.ReadLine();
                        Product product = Search(products, str1);
                        if (product != null)
                        {
                            product.Print();
                            Console.WriteLine("To place an order, enter 'y'");
                            if (Console.ReadLine() == "y")
                                return (user, "NewOrder");
                        }
                        break;

                    case 3:
                        ViewOrders();
                        return CancelOrder(products);

                    case 4:
                        ViewOrders();
                        return ConfirmOrder();

                    case 5:
                        return (user, "Show users");

                    case 6:
                        AddNewProduct(products);
                        break;

                    case 7:
                        ViewProducts(products);
                        СhangeProductValues(products);
                        break;

                    case 8:
                        return (user, "Change order status");

                    case 9:
                        return (user, "LogOut");

                    default:
                        break;
                }
            return (user, "");
        }
    }
}
