﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID_and_UnitTesting
{
    public class Product
    {

        public string Name { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public decimal Cost{ get; set; }
        public Product(string name, string category, string description, decimal cost)
        {
            if (name == null || category == null || description == null)
                throw new NullReferenceException();
            if (name.Length == 0 || category.Length == 0 || description.Length == 0)
                throw new ArgumentException("length ex");
            if (cost <= 0)
                throw new ArgumentException("cost ex");

            Name = name;
            Category = category;
            Description = description;
            Cost = cost;
        }
        public virtual void Print()
        {
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Category: {Category}");
            Console.WriteLine($"Description: {Description}");
            Console.WriteLine($"Cost: {Cost}");
        }
    }
}
