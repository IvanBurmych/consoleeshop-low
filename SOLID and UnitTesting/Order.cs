﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID_and_UnitTesting
{
    public class Order : Product
    {

        private string status;

        public string Status
        {
            get { return status; }
            set
            {
                if (value==null)
                {
                    throw new NullReferenceException();
                }
                if (status != "Received" &&
                    (value == "Canceled by administrator" ||
                    value == "Canceled by User" ||
                    value == "Received Payment" ||
                    value == "Sent" ||
                    value == "Received" ||
                    value == "New")) status = value;
                else throw new ArgumentException("status ex");
            }
        }

        public Order(string name, string category, string description, decimal cost) : base(name, category, description, cost)
        {
            status = "New";
        }
        public Order(Product product) : base(product.Name, product.Category, product.Description, product.Cost)
        {
            status = "New";
        }
        public override void Print()
        {
            Console.WriteLine();
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Category: {Category}");
            Console.WriteLine($"Description: {Description}");
            Console.WriteLine($"Cost: {Cost}");
            Console.WriteLine($"Status: {status}");
            Console.WriteLine();
        }
    }
}
