﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SOLID_and_UnitTesting
{
    public class Program
    {

        static void Main(string[] args)
        {
            List<Product> products = new List<Product> {
            new Product("Bike", "Transport", "fsdfasdfasdfa sdfas", 10000),
            new Product("Car", "Transport", "dasf sdfas", 100000),
            new Product("Fork", "Cooking tool", "dasf fsfds", 100),
            new Product("Spoon", "Cooking tool", "dfsdasf fsfds", 100),
            new Product("Knife", "Cooking tool", "fsdfs fsfffds", 1000),
            new Product("Oven", "Cooking tool", "dfsddasf fsfads", 10000) };

                    List<Order> totalOrders = new List<Order>();

            UsersList users = new UsersList();
            CurrentUser current = new CurrentUser(users[0], products);
            string command = "";
            User newUser;
            void ViewOrders()
            {
                for (int i = 0; i < totalOrders.Count; i++)
                {
                    if (totalOrders[i] == null)
                        throw new ArgumentNullException();
                    Console.WriteLine($"{i}.");
                    totalOrders[i].Print();
                }
                Console.WriteLine();
            }


            while (true)
            {
                (newUser, command) = current.User.PrintActions(current.User, current.ProductsList);
                switch (command)
                {
                    case "LogIn":
                        var res = users.Search(newUser);
                        if (res != null)
                        {
                            current.User = res;
                        }
                        break;

                    case "LogUp":
                        users.Add(new RegisteredUser(newUser.Name, newUser.Pass));
                        break;

                    case "NewOrder":
                        if(newUser is RegisteredUser)
                            totalOrders.Add(((RegisteredUser)newUser).Orders[((RegisteredUser)newUser).Orders.Count - 1]);
                        else if(newUser is AdminUser)
                            totalOrders.Add(((AdminUser)newUser).Orders[((AdminUser)newUser).Orders.Count - 1]);
                        break;

                    case "Canceled by User":
                        List<Order> cancelOrders = null;
                        if (newUser is RegisteredUser)
                            cancelOrders = ((RegisteredUser)newUser).Orders.Where(x => x.Status == "Canceled by User").ToList();
                        else if (newUser is AdminUser)
                            cancelOrders = ((AdminUser)newUser).Orders.Where(x => x.Status == "Canceled by User").ToList();
                        foreach (var item in totalOrders)
                        {
                            foreach (var item2 in cancelOrders)
                            {
                                if (item2.Name==item.Name)
                                {
                                    item.Status = "Canceled by User";
                                }
                            }
                        }
                        break;

                    case "Received":
                        List<Order> receivedOrders = ((RegisteredUser)newUser).Orders.Where(x => x.Status == "Received").ToList();
                        foreach (var item in totalOrders)
                        {
                            foreach (var item2 in receivedOrders)
                            {
                                if (item2.Name == item.Name)
                                {
                                    item.Name = "Received";
                                }
                            }
                        }
                        break;

                    case "Change Name":
                        if (users.SearchByName(newUser.Name) == null)
                        {
                            current.User.Name = newUser.Name;
                        }
                        else
                        {
                            Console.WriteLine("Name is taken ");
                        }
                        break;

                    case "Change Password":
                        current.User.Pass = newUser.Pass;
                        break;

                    case "LogOut":
                        current.User = users[0];
                        break;

                    case "Show users":
                        users.PrintUsersList();
                        Console.WriteLine("Enter the user number to change his data ");
                        string str = Console.ReadLine();
                        int number;
                        bool verif = int.TryParse(str, out number);
                        if (number <= 0 || number > users.Count())
                        {
                            break;
                        }
                        else if (verif)
                        {
                            Console.WriteLine("To change the name enter 1");
                            Console.WriteLine("To change the password enter 2");
                            string str2 = Console.ReadLine();
                            int number2;
                            bool verif2 = int.TryParse(str2, out number2);
                            if (verif2)
                                switch (number2)
                                {
                                    case 1:
                                        users[number].Name = Console.ReadLine();
                                        break;
                                    case 2:
                                        users[number].Pass = Console.ReadLine();
                                        break;
                                    default:
                                        break;
                                }
                        }

                        break;

                    case "Change order status":
                        ViewOrders();
                        Console.WriteLine("To change order status, enter the order number ");
                        string str3 = Console.ReadLine();
                        int number3;
                        bool verif3 = int.TryParse(str3, out number3);
                        if (verif3 && number3 >= 0 && number3 < totalOrders.Count)
                        {
                            Console.WriteLine("To establish the status of an order 'Canceled by administrator' enter'1'");
                            Console.WriteLine("To establish the status of an order 'Received Payment' enter'2'");
                            Console.WriteLine("To establish the status of an order 'Sent' enter'3'");
                            string str4 = Console.ReadLine();
                            int number4;
                            bool verif4 = int.TryParse(str4, out number4);
                            if (verif4)
                                switch (number4)
                                {
                                    case 1:
                                        totalOrders[number3].Status = "Canceled by administrator";
                                        break;
                                    case 2:
                                        totalOrders[number3].Status = "Received Payment";
                                        break;
                                    case 3:
                                        totalOrders[number3].Status = "Sent";
                                        break;

                                    default:
                                        break;
                                }
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
