﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace SOLID_and_UnitTesting
{
    public class GuestUser : User
    {

        public GuestUser() : base() { }

        public override (User, string) PrintActions(User user, List<Product> products)
        {
            Console.WriteLine("Actions:");
            Console.WriteLine("1. Displaying a list of products");
            Console.WriteLine("2. Product search");
            Console.WriteLine("3. LogUp");
            Console.WriteLine("4. LogIn");
            Console.WriteLine("Enter the number");
            int num;
            string read = Console.ReadLine();
            bool res = int.TryParse(read, out num);
            if (res)
                switch (num)
                {
                    case 1:
                        ViewProducts(products);
                        return (user, "");
                    case 2:
                        Console.WriteLine("Enter characters");
                        string str = Console.ReadLine();
                        Product product = Search(products, str);
                        if (product!=null)
                            product.Print();
                        return (user, "");
                    case 3:
                        Console.WriteLine("Enter name");
                        string name = Console.ReadLine();
                        Console.WriteLine("Enter password");
                        string pass = Console.ReadLine();
                        return (new User(name, pass), "LogUp");
                    case 4:
                        Console.WriteLine("Enter name");
                        string name1 = Console.ReadLine();
                        Console.WriteLine("Enter password");
                        string pass1 = Console.ReadLine();
                        return (new User(name1, pass1), "LogIn");
                    default:
                        break;
                }
            return (user, "");
        }
    }
}
