﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID_and_UnitTesting
{
    public class RegisteredUser : User
    {

        public List<Order> Orders { get; set; } = new List<Order>();
        public RegisteredUser(string name, string pass) : base(name, pass) { }
        public void ViewOrders()
        {
            for (int i = 0; i < Orders.Count; i++)
            {
                if (Orders[i] == null)
                    throw new ArgumentNullException();
                Console.WriteLine($"{i}.");
                Orders[i].Print();
            }
            Console.WriteLine();
        }
        public (User, string) ConfirmOrder()
        {
            Console.WriteLine("To confirm receipt, enter the item number ");
            string str = Console.ReadLine();
            int number;
            bool verif = int.TryParse(str, out number);
            if (number < 0 || number >= Orders.Count)
                return (this, "");
            if (verif)
            {
                Orders[number].Status = "Received";
                return (this, "Received");
            }
            return (this, "");
        }
        public (User, string) CancelOrder(List<Product> products)
        {
            Console.WriteLine("To cancel order, enter the item number ");
            string str = Console.ReadLine();
            int number;
            bool verif = int.TryParse(str, out number);
            if (number < 0 || number >= Orders.Count)
                return (this, "");
            if (verif)
            {
                products.Add(new Product(Orders[number].Name, Orders[number].Category, Orders[number].Description, Orders[number].Cost));
                Orders[number].Status = "Canceled by User";
                Console.WriteLine("Complete");
                return (this, "Canceled by User");
            }
            return (this, "");
        }
        public override (User, string) PrintActions(User user, List<Product> products)
        {
            Console.WriteLine("Actions:");
            Console.WriteLine("1. Displaying a list of products");
            Console.WriteLine("2. Product search");
            Console.WriteLine("3. Orders list");
            Console.WriteLine("4. Сonfirm receipt of the item");
            Console.WriteLine("5. Change information about yourself");
            Console.WriteLine("6. LogOut");
            Console.WriteLine("Enter the number");
            Console.WriteLine();
            int num;
            string read = Console.ReadLine();
            bool res = int.TryParse(read, out num);
            if (res)
                switch (num)
                {
                    case 1:
                        ViewProducts(products);
                        Console.WriteLine("To place an order, enter the item number ");
                        Console.WriteLine("Enter any word to return ");
                        string str = Console.ReadLine();
                        int number;
                        bool verif = int.TryParse(str, out number);
                        if (number <= 0 || number > products.Count)
                        {
                            break;
                        }
                        else if (verif)
                        {
                            Orders.Add(new Order(products[number]));
                            products.RemoveAt(number);
                            return (user, "NewOrder");
                        }
                        break;

                    case 2:
                        Console.WriteLine("Enter characters");
                        string str1 = Console.ReadLine();
                        Product product = Search(products, str1);
                        if (product != null)
                        {
                            product.Print();
                            Console.WriteLine("To place an order, enter 'y'");
                            if (Console.ReadLine() == "y")
                                return (user, "NewOrder");
                        }
                        break;

                    case 3:
                        ViewOrders();
                        return CancelOrder(products);

                    case 4:
                        ViewOrders();
                        return ConfirmOrder();

                    case 5:
                        Console.WriteLine("1. Change Name");
                        Console.WriteLine("2. Change Password");
                        Console.WriteLine("Enter the command number");
                        int num5;
                        string read5 = Console.ReadLine();
                        bool res5 = int.TryParse(read5, out num5);
                        if (res5)
                            switch (num5)
                            {
                                case 1:
                                    return (new RegisteredUser(Console.ReadLine(), Pass), "Change Name");
                                case 2:
                                    return (new RegisteredUser(Name, Console.ReadLine()), "Change Password");
                                default:
                                    break;
                            }
                        break;

                    case 6:
                        return (user, "LogOut");

                    default:
                        break;
                }
            return (user, "");
        }
    }
}