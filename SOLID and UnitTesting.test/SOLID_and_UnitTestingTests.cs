using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace SOLID_and_UnitTesting.tests
{
    [TestClass]
    public class SOLID_and_UnitTestingTests
    {

        [TestMethod]
        public void AdminUserClass_OrdersFieldExist()
        {
            string expected = "name";

            AdminUser user = new AdminUser(" ", " ");
            user.Orders.Add((new Order("name", " ", " ", 1)));

            var actual = user.Orders[0].Name;

            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void RegisteredUserClass_ConfirmOrderExist()
        {
            string expected = "name";

            RegisteredUser user = new RegisteredUser(" ", " ");
            user.Orders.Add((new Order("name", " ", " ", 1)));

            var actual = user.Orders[0].Name;

            Assert.AreEqual(expected, actual);
        }




        [TestMethod]
        public void CurrentUserClass_UserFieldExist()
        {
            string expected = "name";

            User user = new User("name", "pass");
            List<Product> products = new List<Product>();
            CurrentUser current = new CurrentUser(user, products);

            var actual = current.User.Name;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CurrentUserClass_ProductsListFieldExist()
        {
            string expected = "name";

            User user = new User(" ", " ");
            List<Product> products = new List<Product>();
            CurrentUser current = new CurrentUser(user, products);
            current.ProductsList.Add(new Product("name", " ", " ", 1));

            var actual = current.ProductsList[0].Name;

            Assert.AreEqual(expected, actual);
        }



        [TestMethod]
        public void UsersListClass_usersFieldExist()
        {
            string expected = "admin";
            UsersList users = new UsersList();

            Assert.AreEqual(expected, users[1].Name);
        }
        [TestMethod]
        public void UsersListClass_AddMethodExist()
        {
            string expected = "name";
            UsersList users = new UsersList();

            users.Add(new User("name", "pass"));

            Assert.AreEqual(expected, users[2].Name);
        }
        [TestMethod]
        public void UsersListClass_SearchMethodExist()
        {
            string expected = "admin";
            UsersList users = new UsersList();
            User user = new User("admin", "admin");

            var actual = users.Search(user);

            Assert.AreEqual(expected, actual.Name);
        }
        [TestMethod]
        public void UsersListClass_CountMethodExist()
        {
            int expected = 2;
            UsersList users = new UsersList();

            var actual = users.Count();

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void UsersListClass_SearchByNameMethodExist()
        {
            string expected = "admin";
            UsersList users = new UsersList();


            var actual = users.SearchByName("admin") ;

            Assert.AreEqual(expected, actual.Name);
        }
        [TestMethod]
        public void UsersListClass_SearchByPassMethodExist()
        {
            string expected = "admin";
            UsersList users = new UsersList();


            var actual = users.SearchByPass("admin");

            Assert.AreEqual(expected, actual.Pass);
        }




        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void UserClass_NullName_ExpextedNullReferenceException()
        {
            User user = new User(null, "pass");
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void UserClass_NullPass_ExpextedNullReferenceException()
        {
            User user = new User("name", null);
        }

        [TestMethod]
        public void UserClass_NameFieldsExist()
        {
            string expected = "name";

            User user = new User("name", "pass");

            Assert.AreEqual(expected, user.Name);
        }

        [TestMethod]
        public void UserClass_PassFieldsExist()
        {
            string expected = "pass";

            User user = new User("name", "pass");

            Assert.AreEqual(expected, user.Pass);
        }
        [TestMethod]
        public void UserClass_SearchMethodExist()
        {
            User user = new User("name", "pass");
            List<Product> products = new List<Product> 
            { 
                new Product(" "," "," ",1),
                new Product("name", "category", "description", 10),
                new Product(" "," "," ",1)
            };
            string expected = "name";

            var actual = user.Search(products, "me");

            Assert.AreEqual(expected, actual.Name);
        }
        [TestMethod]
        public void UserClass_PrintActionsMethodExist()
        {
            string expected = "";
            User user = new User("name", "pass");
            List<Product> products = new List<Product>();

            string actual;
            (user, actual) = user.PrintActions(user, products);

            Assert.AreEqual(expected, actual);
        }




        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void ProductClass_NullName_ExpextedNullReferenceException()
        {
            Product product = new Product(null, "category", "description", 100);
        }
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void ProductClass_NullCategory_ExpextedNullReferenceException()
        {
            Product product = new Product("name", null, "description", 100);
        }
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void ProductClass_NullDescription_ExpextedNullReferenceException()
        {
            Product product = new Product("name", "category", null, 100);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ProductClass_0Cost_ExpextedArgumentException()
        {
            Product product = new Product("name", "category", "description", 0);
        }


        [TestMethod]
        public void ProductClass_NameFieldsExist()
        {
            string expected = "name";

            Product product = new Product("name", "category", "description", 100);

            Assert.AreEqual(expected, product.Name);
        }
        [TestMethod]
        public void ProductClass_CategoryFieldsExist()
        {
            string expected = "category";

            Product product = new Product("name", "category", "description", 100);

            Assert.AreEqual(expected, product.Category);
        }
        [TestMethod]
        public void ProductClass_DescriptionFieldsExist()
        {
            string expected = "description";

            Product product = new Product("name", "category", "description", 100);

            Assert.AreEqual(expected, product.Description);
        }
        [TestMethod]
        public void ProductClass_CostFieldsExist()
        {
            int expected = 100;

            Product product = new Product("name", "category", "description", 100);

            Assert.AreEqual(expected, product.Cost);
        }








        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void OrderClass_NullName_ExpextedNullReferenceException()
        {
            Order order = new Order(null, "category", "description", 100);
        }
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void OrderClass_NullCategory_ExpextedNullReferenceException()
        {
            Order order = new Order("name", null, "description", 100);
        }
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void OrderClass_NullDescription_ExpextedNullReferenceException()
        {
            Order order = new Order("name", "category", null, 100);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void OrderClass_0Cost_ExpextedArgumentException()
        {
            Order order = new Order("name", "category", "description", 0);
        }


        [TestMethod]
        public void OrderClass_NameFieldsExist()
        {
            string expected = "name";

            Order order = new Order("name", "category", "description", 100);

            Assert.AreEqual(expected, order.Name);
        }
        [TestMethod]
        public void OrderClass_CategoryFieldsExist()
        {
            string expected = "category";

            Order order = new Order("name", "category", "description", 100);

            Assert.AreEqual(expected, order.Category);
        }
        [TestMethod]
        public void OrderClass_DescriptionFieldsExist()
        {
            string expected = "description";

            Order order = new Order("name", "category", "description", 100);

            Assert.AreEqual(expected, order.Description);
        }
        [TestMethod]
        public void OrderClass_CostFieldsExist()
        {
            int expected = 100;

            Order order = new Order("name", "category", "description", 100);
            
            Assert.AreEqual(expected, order.Cost);
        }


        [TestMethod]
        public void OrderClass_EmptyStatus_StringNew()
        {
            string expected = "New";

            Order order = new Order(" ", " ", " ", 1);

            Assert.AreEqual(expected, order.Status);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void OrderClass_WrongStatus_ExpextedArgumentException()
        {
            string a = "wrong";

            Order order = new Order(" "," "," ", 1);

            order.Status = a;
        }
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void OrderClass_NullStatus_ExpextedNullReferenceException()
        {
            string a = null;
            Order order = new Order(" ", " ", " ", 1);

            order.Status = a;
        }
    }
}
